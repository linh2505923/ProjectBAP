<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\MembersRequest;
use App\Http\Requests\MembersLoginRequest;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Mail\MailNotify;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view("register");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeForm(MembersRequest $request)
    {
        $data =  $request->all();
        $data['password'] = bcrypt($data['password']);
        if(User::create($data)){
            return redirect()->back()->with('success', __('Register success.'));
        } else {
            return redirect()->back()->withErrors('Register error.');
        
        }
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function formLogin()
    {
        return view("login");
    }
    /**
     * Do login
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkLogin(MembersLoginRequest $request)
    {
        
        $attempt = [
            'email' => $request->email,
            'password' => $request->password,
        ];


        $remember = false;

        if (Auth::attempt($attempt, $remember)) {
            return redirect('/');
        } else {
            return redirect()->back()->withErrors('Email or password is not correct.');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function show(Members $members)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function edit(Members $members)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Members $members)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
       
        Auth::logout();
        return redirect('/');
       
    }
    public function formResetPass(){
        return view("reset_password");
    }


    public function resetPass(Request $request)
    {
        $getId = User::where("email", $request->email)->get()->toArray();
        

        $data = [
            'subject' => 'Reset password',
            'id_user' => $getId[0]['id']
        ];



        try {
            Mail::to($request->email)->send(new MailNotify($data));
            return response()->json(['Send mail success']);
        } catch (Exception $th) {
            return response()->json(['Send mail error']);
        }
    }

    public function formChangePass($id)
    {
        $idUser =  $id;
        return view('change_pass',compact("idUser"));
       
    }
    public function changePass(Request $request, $id)
    {
        $data = User::find($id);
        $data->password= bcrypt($request->password);
        if($data->save()){
            return redirect()->back()->with('success', __('change password success.'));
        
        } else {
            return redirect()->back()->withErrors('change password error.');
        
        }
    }
}
