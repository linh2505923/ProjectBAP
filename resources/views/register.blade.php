<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style type="text/css">
		.main{
			width: 500px;
			display: block;
			margin: 0 auto;
		}
		
		.main div{
			width: 100%;
			display: inline-block;
		}
		.main label{
			width: 120px;
			float: left;
			text-align: left;
		}

		h4, p, li{
			color: red;
		}
	</style>
</head>
<body>
	<div class="main">
		@if(session('success'))
       		<h4><i class="icon fa fa-check"></i> Thông báo!</h4>
            <p>{{session('success')}}</p>
            <p>Click vao <a href="{{ url('/member/login')}}">đây</a> để sang trang login.</p>
        @endif
        @if($errors->any())
            
            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
            
        @endif

		<h1>Form register:</h1>
		<form method="post" action="">
			@csrf
			<div>
				<label>name</label>
				<input type="text" name="name">
			</div>
			<div>
				<label>email</label>
				<input type="text" name="email">
			</div>
			<div>
				<label>pass</label>
				<input type="password" name="password">
			</div>
			<div>
				
				<button type="submit">submit</button>
			</div>
			
		</form>
	</div>

</body>
</html>