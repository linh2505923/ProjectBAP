
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<h1>Welcome laravel!</h1>
	<ul>
		<?php 
			if(Auth::check()){
		?>
			<li><a href="{{ url('/member/logout')}}">Logout</a></li>
			
		<?php
			}else{
		?>
			<li><a href="{{ url('/member/login')}}">Login</a></li>
			<li><a href="{{ url('/member/register')}}">Register</a></li>
		<?php 
			}
		?>
		

		
		<li><a href="{{ url('/member/reset-password')}}">Reset password</a></li>
	</ul>

</body>
</html>