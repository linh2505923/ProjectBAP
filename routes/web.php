<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::get('/member/register',[UserController::class, 'createForm']);
Route::post('/member/register',[UserController::class, 'storeForm']);

Route::get('/member/login',[UserController::class, 'formLogin']);
Route::post('/member/login',[UserController::class, 'checkLogin']);

Route::get('/member/logout',[UserController::class, 'destroy']);

Route::get('/member/reset-password',[UserController::class, 'formResetPass']);
Route::post('/member/reset-password',[UserController::class, 'resetPass']);


Route::get('/member/change-password/{id}',[UserController::class, 'formChangePass']);
Route::post('/member/change-password/{id}',[UserController::class, 'changePass']);
